 package org.apache.cordova.FbDeferredDeepLinking;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;


import com.facebook.AppLinkData;
import android.util.Log;

/**
 * This class echoes a string called from JavaScript.
 */
public class FbDeferredDeepLinking extends CordovaPlugin {

    private static final String ACTION_CHECKINTENT = "checkFbDeferredLinking";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (ACTION_CHECKINTENT.equalsIgnoreCase(action)) {
            this.checkFbDeferredDeepLinking(callbackContext);
            return true;
        }
        return false;
    }

    private void checkFbDeferredDeepLinking(CallbackContext callbackContext) {
        AppLinkData.fetchDeferredAppLinkData(((CordovaActivity) this.webView.getContext()),
          new AppLinkData.CompletionHandler() {
            @Override
            public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
              try {
                String deeplink = appLinkData.getTargetUri().toString();
                Log.w(ACTION_CHECKINTENT, "Deferred App Link Target URL: " + deeplink);
                final StringWriter writer = new StringWriter(deeplink.length() * 2);
                escapeJavaStyleString(writer, deeplink, true, false);
                cordova.getActivity().runOnUiThread(new Runnable() {
                  public void run() {
                    webView.loadUrl("javascript:handleOpenURL('" + writer.toString() + "');");
                  }
                });
              } catch (Exception ignore) {
                Log.w(ACTION_CHECKINTENT, "Deferred App Link Target Null");
              }
            }
          }
      );
    }


  // Taken from commons StringEscapeUtils
  private static void escapeJavaStyleString(Writer out, String str, boolean escapeSingleQuote,
                                            boolean escapeForwardSlash) throws IOException {
    if (out == null) {
      throw new IllegalArgumentException("The Writer must not be null");
    }
    if (str == null) {
      return;
    }
    int sz;
    sz = str.length();
    for (int i = 0; i < sz; i++) {
      char ch = str.charAt(i);

      // handle unicode
      if (ch > 0xfff) {
        out.write("\\u" + hex(ch));
      } else if (ch > 0xff) {
        out.write("\\u0" + hex(ch));
      } else if (ch > 0x7f) {
        out.write("\\u00" + hex(ch));
      } else if (ch < 32) {
        switch (ch) {
          case '\b':
            out.write('\\');
            out.write('b');
            break;
          case '\n':
            out.write('\\');
            out.write('n');
            break;
          case '\t':
            out.write('\\');
            out.write('t');
            break;
          case '\f':
            out.write('\\');
            out.write('f');
            break;
          case '\r':
            out.write('\\');
            out.write('r');
            break;
          default:
            if (ch > 0xf) {
              out.write("\\u00" + hex(ch));
            } else {
              out.write("\\u000" + hex(ch));
            }
            break;
        }
      } else {
        switch (ch) {
          case '\'':
            if (escapeSingleQuote) {
              out.write('\\');
            }
            out.write('\'');
            break;
          case '"':
            out.write('\\');
            out.write('"');
            break;
          case '\\':
            out.write('\\');
            out.write('\\');
            break;
          case '/':
            if (escapeForwardSlash) {
              out.write('\\');
            }
            out.write('/');
            break;
          default:
            out.write(ch);
            break;
        }
      }
    }
  }

  private static String hex(char ch) {
    return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
  }

}
